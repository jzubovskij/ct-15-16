package gen;

import ast.ASTVisitor;
import ast.Assign;
import ast.BinOp;
import ast.Block;
import ast.ChrLiteral;
import ast.Expr;
import ast.FunCallExpr;
import ast.FunCallStmt;
import ast.If;
import ast.IntLiteral;
import ast.Op;
import ast.Procedure;
import ast.Program;
import ast.Return;
import ast.Stmt;
import ast.StrLiteral;
import ast.Type;
import ast.Var;
import ast.VarDecl;
import ast.While;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.locks.Condition;

import org.objectweb.asm.*;

public class CodeGenerator implements ASTVisitor {

	ClassWriter cw;
	MethodVisitor mv;
	FieldVisitor fv;
	AnnotationVisitor av;
	Procedure current;

	Op comparison;

	// do not need visit source as have only one java file at a time
	public void emitProgram(Program program) {

		cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		cw.visit(Opcodes.V1_7, Opcodes.ACC_PUBLIC, "Main", null, "java/lang/Object", null);
		program.accept(this);
		cw.visitEnd();
		writeOutput(cw.toByteArray());

	}

	@Override
	public Object visitBlock(Block b) {
		for (Stmt stmt : b.statements) {
			stmt.accept(this);
		}

		return null;
	}

	@Override
	public Object visitProcedure(Procedure p) {

		current = p;
		p.printVariables();

		if (p.name.equals("main")) {
			mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);

		} else {
			String paramType = "";
			for (VarDecl vd : p.params) {

				paramType += vd.getType();

			}

			mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, p.name, "(" + paramType + ")" + p.getType(),
					null, null);
		}

		// visit block
		mv.visitCode();
		p.block.accept(this);

		mv.visitInsn(Opcodes.RETURN);
		// computing everything automatically
		mv.visitMaxs(0, 0);
		mv.visitEnd();

		return null;
	}

	@Override
	public Object visitProgram(Program program) {
		for (VarDecl vd : program.varDecls) {
			vd.accept(this);
		}

		for (Procedure proc : program.procs) {
			proc.accept(this);

		}
		program.main.accept(this);

		return null;
	}

	@Override
	public Object visitVarDecl(VarDecl vd) {
		// do not need to visit local ones
		if (vd.global) {
			fv = cw.visitField(Opcodes.ACC_STATIC, vd.var.name, vd.getType(), null, null);
			fv.visitEnd();
		}
		return null;
	}

	@Override
	public Object visitVar(Var v) {
		// unless it is an assign, we pop its value
		if (v.getVd().global) {
			mv.visitFieldInsn(Opcodes.GETSTATIC, "Main", v.name, v.getVd().getType());
		} else
			mv.visitVarInsn(Opcodes.ILOAD, v.getVd().index);
		return null;
	}

	@Override
	public Object visitChrLiteral(ChrLiteral chrLiteral) {
		mv.visitIntInsn(Opcodes.BIPUSH, (int) chrLiteral.character);
		return null;
	}

	@Override
	public Object visitStrLiteral(StrLiteral strLiteral) {
		mv.visitLdcInsn(strLiteral.string);
		return null;
	}

	@Override
	public Object visitIntLiteral(IntLiteral intLiteral) {
		if (intLiteral.integer < 32768) {
			mv.visitIntInsn(Opcodes.SIPUSH, intLiteral.integer);
		} else {
			mv.visitLdcInsn(new Integer(intLiteral.integer));
		}

		return null;
	}

	@Override
	public Object visitBinOp(BinOp binOp) {
		binOp.operandOne.accept(this);
		binOp.operandTwo.accept(this);
		if (binOp.operator == Op.ADD) {
			mv.visitInsn(Opcodes.IADD);
		} else if (binOp.operator == Op.SUB) {
			mv.visitInsn(Opcodes.ISUB);
		} else if (binOp.operator == Op.MUL) {
			mv.visitInsn(Opcodes.IMUL);
		} else if (binOp.operator == Op.DIV) {
			mv.visitInsn(Opcodes.IDIV);
		} else if (binOp.operator == Op.MOD) {
			mv.visitInsn(Opcodes.IREM);
		} else {
			comparison = binOp.operator;
		}

		return null;
	}

	@Override
	public Object visitFunCallExpr(FunCallExpr funCallExpr) {

		String paramType = "";
		for (VarDecl vd : funCallExpr.getP().params) {

			paramType += vd.getType();

		}

		for (Expr expr : funCallExpr.arguments) {
			expr.accept(this);
		}
		String callClass = "Main";
		if (funCallExpr.name.equals("print_i") || funCallExpr.name.equals("print_c")
				|| funCallExpr.name.equals("print_s") || funCallExpr.name.equals("read_c")
				|| funCallExpr.name.equals("read_i")) {

			callClass = "IO";

		}
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, callClass, funCallExpr.name,
				"(" + paramType + ")" + funCallExpr.getP().getType());
		return null;
	}

	@Override
	public Object visitWhile(While while1) {

		Label ifTrue = new Label();
		Label ifFalse = new Label();

		mv.visitLabel(ifTrue);

		comparison = null;
		while1.condition.accept(this);
		Op localOp = comparison;

		if (localOp != null) {
			if (Op.EQ == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPNE, ifFalse);
			} else if (Op.NE == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPEQ, ifFalse);
			} else if (Op.GE == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPLT, ifFalse);
			} else if (Op.LE == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPGT, ifFalse);
			} else if (Op.LT == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPGE, ifFalse);
			} else if (Op.GT == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPLE, ifFalse);
			}
		} else {
			mv.visitJumpInsn(Opcodes.IFEQ, ifFalse);
		}

		while1.actions.accept(this);
		mv.visitJumpInsn(Opcodes.GOTO, ifTrue);
		mv.visitLabel(ifFalse);

		return null;
	}

	@Override
	public Object visitIf(If if1) {
		Label ifTrue = new Label();
		Label ifFalse = new Label();

		comparison = null;
		if1.condition.accept(this);
		Op localOp = comparison;

		if (localOp != null) {
			if (Op.EQ == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPNE, ifFalse);
			} else if (Op.NE == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPEQ, ifFalse);
			} else if (Op.GE == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPLT, ifFalse);
			} else if (Op.LE == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPGT, ifFalse);
			} else if (Op.LT == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPGE, ifFalse);
			} else if (Op.GT == localOp) {
				mv.visitJumpInsn(Opcodes.IF_ICMPLE, ifFalse);
			}
		} else {
			mv.visitJumpInsn(Opcodes.IFEQ, ifFalse);
		}

		if1.actions.accept(this);
		mv.visitJumpInsn(Opcodes.GOTO, ifTrue);

		mv.visitLabel(ifFalse);
		if (if1.elseCondition != null) {
			if1.elseCondition.accept(this);
		}
		mv.visitLabel(ifTrue);
		return null;
	}

	@Override
	public Object visitAssign(Assign assign) {
		assign.expression.accept(this);
		if (assign.var.getVd().global) {
			mv.visitFieldInsn(Opcodes.PUTSTATIC, "Main", assign.var.name, assign.var.getVd().getType());
		} else {
			mv.visitVarInsn(Opcodes.ISTORE, assign.var.getVd().index);
		}
		return null;
	}

	@Override
	public Object visitReturn(Return return1) {
		// TODO check
		if (return1.expr != null) {

			return1.expr.accept(this);
			if (current.type == Type.CHAR) {
				mv.visitInsn(Opcodes.IRETURN);
			} else if (current.type == Type.INT) {
				mv.visitInsn(Opcodes.IRETURN);
			} else {
				mv.visitInsn(Opcodes.RETURN);
			}

		}
		return null;
	}

	@Override
	public Object visitFunCallStmt(FunCallStmt funCallStmt) {
		for (Expr expr : funCallStmt.arguments) {
			expr.accept(this);
		}

		String paramType = "";
		for (VarDecl vd : funCallStmt.getP().params) {

			paramType += vd.getType();

		}
		String callClass = "Main";
		if (funCallStmt.name.equals("print_i") || funCallStmt.name.equals("print_c")
				|| funCallStmt.name.equals("print_s") || funCallStmt.name.equals("read_c")
				|| funCallStmt.name.equals("read_i")) {

			callClass = "IO";

		}
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, callClass, funCallStmt.name,
				"(" + paramType + ")" + funCallStmt.getP().getType());
		// need to do this as it puts this value on top of the stack
		
		
		if (funCallStmt.getP().type != Type.VOID && funCallStmt.getP().type != null) {
			mv.visitInsn(Opcodes.POP);
		}
		return null;
	}

	void writeOutput(byte[] byteArray) {
		FileOutputStream output;
		try {
			output = new FileOutputStream("out/Main.class");
			output.write(byteArray);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
