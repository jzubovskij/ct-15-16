package sem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.TabableView;

import ast.Block;
import ast.Procedure;
import ast.Stmt;
import ast.Type;
import ast.Var;
import ast.VarDecl;

public class Scope {
	private Scope outer;
	public Map<String, Symbol> symbolTable;

	public Scope(Scope outer) {
		symbolTable = new HashMap<String, Symbol>();
		this.outer = outer;

	}

	public Scope() {
		this(null);
	}

	public Symbol lookup(String name) {

		return (symbolTable.containsKey(name)) ? lookupCurrent(name)
				: (outer != null) ? outer.lookup(name) : null;
	}

	public Symbol lookupCurrent(String name) {

		try {
			return symbolTable.get(name);
		} catch (Exception e) {
			return null;
		}
	}

	public void put(Symbol sym) {
		symbolTable.put(sym.name, sym);

	}
	

	
}
