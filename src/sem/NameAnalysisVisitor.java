package sem;

import java.util.ArrayList;
import java.util.List;

import ast.*;

public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {

	Scope scope;
	List<Symbol> fromOuter;
	boolean passToInner;

	Procedure current;

	public Void visitProgram(Program p) {
		// make outer layer null
		scope = new Scope();

		defineReadC();
		defineReadI();
		definePrintI();
		definePrintC();
		definePrintS();

		int index = 1;
		for (VarDecl vd : p.varDecls) {
			vd.global = true;
			vd.accept(this);
			vd.index = index;
			index++;
		}

		for (Procedure proc : p.procs) {
			proc.accept(this);
		}
		p.main.accept(this);
		return null;
	}

	public Void visitBlock(Block b) {

		Scope oldScope = scope;
		scope = new Scope(scope);

		if (fromOuter != null && passToInner) {
			for (Symbol sym : fromOuter) {
				scope.put(sym);

			}
		}
		passToInner = false;
		fromOuter.clear();

		for (VarDecl vd : b.variables) {
			vd.accept(this);
		}

		for (Stmt stmt : b.statements) {
			stmt.accept(this);
		}

		scope = oldScope;
		return null;
	}

	public Void visitVarDecl(VarDecl vd) {
		Symbol s = scope.lookupCurrent(vd.var.name);

		if (s != null) {
			error("Namespace used in Scope: " + vd.var.name + "\n");
			return null;
		} else {
			if (!vd.global) {
				vd.index = current.index;
				current.putVardiableDeclaration(vd);
			}
			scope.put(new VarSymbol(vd));

		}

		return null;
	}

	public Void visitProcedure(Procedure p) {
		Symbol s = scope.lookupCurrent(p.name);
		current = p;
		if (s != null) {
			error("Namespace used in Scope: " + p.name + "\n");
			return null;
		} else {
			scope.put(new ProcSymbol(p));
		}

		List<Symbol> parameters = new ArrayList<Symbol>();
		for (VarDecl vd : p.params) {
			parameters.add(new VarSymbol(vd));
			passToInner = true;
			//ADD TO THE PROCEDURE
			vd.index = current.index;
			current.putVardiableDeclaration(vd);
		}
		fromOuter = parameters;
		
		

		p.block.accept(this);

		
		
		p.variables.putAll(current.variables);

		return null;
	}

	@Override
	public Void visitBinOp(BinOp binOp) {

		binOp.operandOne.accept(this);
		binOp.operandTwo.accept(this);

		return null;
	}

	public Void visitVar(Var v) {
		Symbol s = scope.lookup(v.name);
		if (s == null) {
			error("Inexistent VarDecl: " + v.name + "\n");
			return null;

		}
		if (s.getClass() != VarSymbol.class) {
			error("Procedure found, Expected VarDecl: " + v.name + "\n");
			return null;
		} else {
			v.setVd(((VarSymbol) s).vd);
		}

		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr funCallExpr) {
		Symbol s = scope.lookup(funCallExpr.name);

		if (s == null) {
			error("Inexistent Procedure declaration: " + funCallExpr.name + "\n");
			return null;
		}

		if (s.getClass() == VarSymbol.class) {
			error("VarDecl found, Expected Procedure: " + funCallExpr.name + "\n");
			return null;
		} else {
			for (Expr expr : funCallExpr.arguments) {
				expr.accept(this);
			}
			funCallExpr.setP(((ProcSymbol) s).p);

		}

		return null;
	}

	@Override
	public Void visitFunCallStmt(FunCallStmt funCallStmt) {
		Symbol s = scope.lookup(funCallStmt.name);

		if (s == null) {
			error("Inexistent Procedure declaration: " + funCallStmt.name + "\n");
			return null;
		}

		if (s.getClass() == VarSymbol.class) {
			error("VarDecl found, Expected Procedure: " + funCallStmt.name + "\n");
			return null;
		} else {

			for (Expr expr : funCallStmt.arguments) {
				expr.accept(this);
			}
			funCallStmt.setP(((ProcSymbol) s).p);
		}

		return null;
	}

	@Override
	public Void visitWhile(While while1) {
		// TODO do IF and WHILE have separate scopes
		Scope oldScope = scope;
		scope = new Scope(scope);

		while1.condition.accept(this);

		while1.actions.accept(this);

		scope = oldScope;
		return null;
	}

	@Override
	public Void visitIf(If if1) {
		Scope oldScope = scope;
		scope = new Scope(scope);

		if1.condition.accept(this);
		if (if1.actions != null) {
			if1.actions.accept(this);
		}
		if (if1.elseCondition != null) {
			if1.elseCondition.accept(this);
		}

		scope = oldScope;
		return null;
	}

	@Override
	public Void visitAssign(Assign assign) {

		assign.var.accept(this);
		assign.expression.accept(this);

		return null;
	}

	@Override
	public Void visitReturn(Return return1) {
		if (return1.expr != null) {
			return1.expr.accept(this);
		}
		return null;
	}

	@Override
	public Void visitChrLiteral(ChrLiteral chrLiteral) {
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral strLiteral) {
		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral intLiteral) {
		return null;
	}

	private void defineReadI() {
		Procedure read_i;

		Block emptyBlock = new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>());
		List<VarDecl> parameters = new ArrayList<VarDecl>();

		read_i = new Procedure(Type.INT, "read_i", parameters, emptyBlock);
		scope.put(new ProcSymbol(read_i));

	}

	private void defineReadC() {
		Procedure read_c;

		Block emptyBlock = new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>());
		List<VarDecl> parameters = new ArrayList<VarDecl>();

		read_c = new Procedure(Type.CHAR, "read_c", parameters, emptyBlock);
		scope.put(new ProcSymbol(read_c));
	}

	private void definePrintI() {
		Procedure print_i;

		Block emptyBlock = new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>());
		List<VarDecl> parameters = new ArrayList<VarDecl>();

		parameters.add(new VarDecl(Type.INT, new Var("toPrint")));

		print_i = new Procedure(Type.VOID, "print_i", parameters, emptyBlock);
		scope.put(new ProcSymbol(print_i));
	}

	private void definePrintC() {
		Procedure print_c;

		Block emptyBlock = new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>());
		List<VarDecl> parameters = new ArrayList<VarDecl>();

		parameters.add(new VarDecl(Type.CHAR, new Var("toPrint")));

		print_c = new Procedure(Type.VOID, "print_c", parameters, emptyBlock);
		scope.put(new ProcSymbol(print_c));

	}

	private void definePrintS() {
		Procedure print_s;

		Block emptyBlock = new Block(new ArrayList<VarDecl>(), new ArrayList<Stmt>());
		List<VarDecl> parameters = new ArrayList<VarDecl>();

		parameters.add(new VarDecl(Type.STRING, new Var("toPrint")));

		print_s = new Procedure(Type.VOID, "print_s", parameters, emptyBlock);
		scope.put(new ProcSymbol(print_s));

	}

}
