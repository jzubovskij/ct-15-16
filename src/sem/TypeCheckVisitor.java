package sem;

import java.util.ArrayList;
import java.util.List;

import ast.*;

public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {

	private Procedure current;
	boolean returnFound;
	
	public Type visitProgram(Program p) {
		for (VarDecl vd : p.varDecls) {
			vd.accept(this);
		}

		for (Procedure proc : p.procs) {
			proc.accept(this);
		}
		p.main.accept(this);
		return null;
	}

	public Type visitBlock(Block b) {

		for (VarDecl vd : b.variables) {
			vd.accept(this);
		}
		for (Stmt stmt : b.statements) {
			stmt.accept(this);
		}

		return null;
	}

	public Type visitProcedure(Procedure p) {

		current = p;
		returnFound = false;
		
		for (VarDecl vd : p.params) {
			vd.accept(this);
		}
		p.block.accept(this);
		if(p.type != Type.VOID && p.type != null)
		{
			if(!returnFound)
			{
				error("Return statement Not Found for Procedure: " + p.name);
			}
		}
		
        returnFound = false;
        current = null;
		return null;
	}

	// TODO check if conditional needed for IF and WHILE
	@Override
	public Type visitWhile(While while1) {

		Type type = while1.condition.accept(this);
		if (type != Type.INT) {
			error("Non-Boolean Expression in WHILE conditional statement");
		}
		return while1.actions.accept(this);

	}

	@Override
	public Type visitIf(If if1) {

		Type type = if1.condition.accept(this);
		if (type != Type.INT) {
			error("Non-Boolean Expression in IF conditional statement");
		}
		Type typeOne = null, typeTwo = null;
		typeOne = if1.actions.accept(this);

		if (if1.elseCondition != null) {
			typeTwo = if1.elseCondition.accept(this);
		}

		if (typeTwo != null) {
			if (typeOne != typeTwo) {
				error("True and False condition Return Type Mismatch");
			}
		}
		return typeOne;

	}

	@Override
	public Type visitAssign(Assign assign) {

		Type typeOne = assign.var.accept(this);
		Type typeTwo = assign.expression.accept(this);
		if (typeTwo != typeOne) {
			error("Assignment Type Mismatch: " + typeOne + " = " + typeTwo);
		}

		return null;
	}

	@Override
	public Type visitReturn(Return return1) {
		
		returnFound = true;
		
		Type returnType = null;
		if (return1.expr != null) {
			returnType = return1.expr.accept(this);
		}
		
		Type procType = (current.type == Type.VOID || current.type == null) ? null : current.type;
		if(returnType != procType )
		{
			error("Return statement and Procedure type Mismatch: " + current.name
					+ " : " + returnType + " vs " + current.type);
		}
		return returnType;
	}

	@Override
	public Type visitFunCallExpr(FunCallExpr funCallExpr) {
		List<VarDecl> parameters = new ArrayList<VarDecl>();
		try {
			parameters.addAll(funCallExpr.getP().params);
		} catch (Exception e) {

		}

		if (parameters.size() != funCallExpr.arguments.size()) {
			error("Argument Number Mismatch for Procedure: " + funCallExpr.name
					+ ": " + parameters.size() + " vs "
					+ funCallExpr.arguments.size());
		} else {
			int index = 0;
			for (Expr expr : funCallExpr.arguments) {
				Type argumentType = expr.accept(this);

				if (argumentType != parameters.get(index).type) {

					error("Argument and Parameter Type Mismatch :"
							+ funCallExpr.name + " : " + argumentType + " vs "
							+ parameters.get(index).type);
				}

				index++;
			}
		}
		Type type = null;
		try {
			type = funCallExpr.getP().type;
			if (!(type == Type.INT || type == Type.CHAR)) {
				error("Function Call Expression Type not Supported: "
						+ funCallExpr.getP().name);
			}
		} catch (Exception e) {

		}

		return type;
	}

	@Override
	public Type visitFunCallStmt(FunCallStmt funCallStmt) {
		List<VarDecl> parameters = new ArrayList<VarDecl>();
		try {
			parameters.addAll(funCallStmt.getP().params);
		} catch (Exception e) {

		}
		if (parameters.size() != funCallStmt.arguments.size()) {
			error("Argument Number Mismatch for Procedure: " + funCallStmt.name
					+ ": " + parameters.size() + " vs "
					+ funCallStmt.arguments.size());
		} else {
			int index = 0;
			for (Expr expr : funCallStmt.arguments) {
				Type argumentType = expr.accept(this);

				if (argumentType != parameters.get(index).type) {
					error("Argument and Parameter Type Mismatch :"
							+ funCallStmt.name + " : " + argumentType + " vs "
							+ parameters.get(index).type);
				}

				index++;
			}
		}

		return null;
	}

	@Override
	public Type visitBinOp(BinOp binOp) {

		Type typeOne = binOp.operandOne.accept(this);
		Type typeTwo = binOp.operandTwo.accept(this);

		if (typeOne != typeTwo) {
			error("Operand Type Mismatch: " + typeOne + " to " + typeTwo);
		}

		if (binOp.operator == Op.DIV || binOp.operator == Op.MOD
				|| binOp.operator == Op.SUB || binOp.operator == Op.ADD) {
			if (typeOne != Type.INT) {
				error("Arithmetic BinOp is not an INT");
			}
		}

		return Type.INT;
	}

	public Type visitVar(Var v) {
		if (v.getVd() != null) {
			return v.getVd().type;
		} else
			return null;
	}

	public Type visitVarDecl(VarDecl vd) {
		Type result = vd.type;
		if (result == Type.VOID || result == Type.STRING) {
			error("Variable of Incorrect type: " + result);
		}
		return result;
	}

	@Override
	public Type visitChrLiteral(ChrLiteral chrLiteral) {
		return Type.CHAR;
	}

	@Override
	public Type visitStrLiteral(StrLiteral strLiteral) {
		return Type.STRING;
	}

	@Override
	public Type visitIntLiteral(IntLiteral intLiteral) {

		return Type.INT;
	}
}
