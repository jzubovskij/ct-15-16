package sem;

import ast.Procedure;

public class ProcSymbol extends Symbol {
	Procedure p;

	public ProcSymbol(Procedure p) {
		super(p.name);
		this.p = p;
	}

}