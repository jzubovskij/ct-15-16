package parser;

import ast.Assign;
import ast.BinOp;
import ast.Block;
import ast.ChrLiteral;
import ast.Expr;
import ast.FunCallExpr;
import ast.FunCallStmt;
import ast.If;
import ast.IntLiteral;
import ast.Op;
import ast.Procedure;
import ast.Program;
import ast.Return;
import ast.Stmt;
import ast.StrLiteral;
import ast.Type;
import ast.Var;
import ast.VarDecl;
import ast.While;
import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//I AM THE AUTHOR OF DONUTS
public class Parser {

	private Token token;

	private Queue<Token> buffer = new LinkedList<>();

	private final Tokeniser tokeniser;

	public Parser(Tokeniser tokeniser) {
		this.tokeniser = tokeniser;
	}

	public Program parse() {
		nextToken();
		return parseProgram();
	}

	public int getErrorCount() {
		return error;
	}

	private int error = 0;
	private Token lastErrorToken;

	private void error(TokenClass... expected) {

		if (lastErrorToken == token) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		String sep = "";
		for (TokenClass e : expected) {
			sb.append(sep);
			sb.append(e);
			sep = "|";
		}
		System.out.println("Parsing error: expected ("+sb+") found ("+token+") at "+token.position);
		error++;
		
		lastErrorToken = token;
	}

	private Token lookAhead(int i) {
		while (buffer.size() < i)
			buffer.add(tokeniser.nextToken());
		assert buffer.size() >= i;

		int cnt = 1;
		for (Token t : buffer) {
			if (cnt == i)
				return t;
			cnt++;
		}

		assert false;
		return null;
	}

	private void nextToken() {
		if (!buffer.isEmpty())
			token = buffer.remove();
		else
			token = tokeniser.nextToken();
	}

	private Token expect(TokenClass... expected) {
		for (TokenClass e : expected) {
			if (e == token.tokenClass) {
				Token cur = token;
				nextToken();
				return cur;
			}
		}

		error(expected);
		return null;
	}

	private boolean accept(TokenClass... expected) {
		boolean result = false;
		for (TokenClass e : expected)
			result |= (e == token.tokenClass);
		return result;
	}

	private Program parseProgram() {
		parseIncludes();
		List<VarDecl> varDecls = parseVarDecls();
		List<Procedure> procs = parseProcs();
		Procedure main = parseMain();
		expect(TokenClass.EOF);
		return new Program(varDecls, procs, main);
		// return null;
	}

	private void parseIncludes() {
		// includes_rep :: = includes_rep ("#include" STRING_LITERAL) | ϵ
		if (accept(TokenClass.INCLUDE)) {
			nextToken();
			expect(TokenClass.STRING_LITERAL);
			parseIncludes();
		}
		// //System.out.printf("parsing INCLUDES FINISHED at TOKEN : %s \n",
		// token.tokenClass.toString());
	}

	private List<VarDecl> parseVarDecls() {
		// vardecls_rep = vardecls_rep (typeident ";") | ϵ
		List<VarDecl> result = new ArrayList<VarDecl>();
		if (accept(TokenClass.VOID, TokenClass.INT, TokenClass.CHAR)) {
			// do not parse method delcarations
			if (lookAhead(1).tokenClass != TokenClass.MAIN && lookAhead(2).tokenClass != TokenClass.LPAR) {
				// System.out.printf("parsing VAR parsing / CONTINUED ON : %s
				// \n", token.tokenClass.toString());
				Type type = null;
				Token typeId = token;
				nextToken();
				if (typeId.tokenClass == TokenClass.VOID) {
					type = Type.VOID;
				}
				if (typeId.tokenClass == TokenClass.INT) {
					type = Type.INT;
				}
				if (typeId.tokenClass == TokenClass.CHAR) {
					type = Type.CHAR;
				}
				Token identifier = token;
				expect(TokenClass.IDENTIFIER);
				expect(TokenClass.SEMICOLON);
				VarDecl declaration = new VarDecl(type, new Var(identifier.data));
				result.add(declaration);
				if (accept(TokenClass.VOID, TokenClass.INT, TokenClass.CHAR)) {
					result.addAll(parseVarDecls());
				}
			}
		}
		return result;

	}

	private List<Procedure> parseProcs() {
		// procedure_opt (type IDENT "(" params_opt ")" body)| ϵ
		List<Procedure> result = new ArrayList<Procedure>();
		if (accept(TokenClass.VOID, TokenClass.INT, TokenClass.CHAR)) {
			if (lookAhead(1).tokenClass != TokenClass.MAIN) {
				Type type = null;
				if (token.tokenClass == TokenClass.VOID) {
					type = Type.VOID;
				}
				if (token.tokenClass == TokenClass.INT) {
					type = Type.INT;
				}
				if (token.tokenClass == TokenClass.CHAR) {
					type = Type.CHAR;
				}
				nextToken();
				Token identifier = token;
				expect(TokenClass.IDENTIFIER);
				expect(TokenClass.LPAR);
				List<VarDecl> params = new ArrayList<VarDecl>();
				// params_opt
				if (accept(TokenClass.VOID, TokenClass.INT, TokenClass.CHAR)) {
					params.addAll(parseParams());
				}
				expect(TokenClass.RPAR);
				Block body = parseBody();
				Procedure procedure = new Procedure(type, identifier.data, params, body);
				result.add(procedure);
				result.addAll(parseProcs());
			}
		}
		return result;
		// System.out.printf("parsing PROCS FINISHED at TOKEN : %s \n",
		// token.tokenClass.toString());
	}

	private List<VarDecl> parseParams() {
		
		List<VarDecl> result = new ArrayList<VarDecl>();
		Token typeId = token;
		expect(TokenClass.INT, TokenClass.VOID, TokenClass.CHAR);
		Type type = null;
		if (typeId.tokenClass == TokenClass.VOID) {
			type = Type.VOID;
		}
		else if (typeId.tokenClass == TokenClass.INT) {
			type = Type.INT;
		}
		else if (typeId.tokenClass == TokenClass.CHAR) {
			type = Type.CHAR;
		}
		
		Token identifier = token;
		expect(TokenClass.IDENTIFIER);
		VarDecl parameter = new VarDecl(type, new Var(identifier.data));
		result.add(parameter);
		if (accept(TokenClass.COMMA)) {
			nextToken();
			result.addAll(parseParams());
		}
		return result;

	}

	private Block parseBody() {
		// body ::= "{" vardecls_rep stmt_rep "}"
		expect(TokenClass.LBRA);
		List<VarDecl> variables = parseVarDecls();
		List<Stmt> statements = parseStatementRep();
		expect(TokenClass.RBRA);
		return new Block(variables, statements);
		// System.out.printf("parsing BODY FINISHED at TOKEN : %s \n",
		// token.tokenClass.toString());
	}

	private List<Stmt> parseStatementRep() {
		List<Stmt> result = new ArrayList<Stmt>();
		if (accept(TokenClass.LBRA, TokenClass.WHILE, TokenClass.IF, TokenClass.IDENTIFIER, TokenClass.RETURN,
				TokenClass.PRINT, TokenClass.READ) || isFuncall()) {
			result.add(parseStatement());
			result.addAll(parseStatementRep());
		}
		// stmt_rep :: = stmt_rep stmt | ϵ
		return result;

	}

	private Stmt parseStatement() {
		/*
		 * stmt ::= "{" vardecls_rep stmt_rep "}" | "while" "(" exp ")" stmt |
		 * "if" "(" exp ")" stmt else_opt | IDENT "=" lexp ";" | "return"
		 * lexp_opt ";" | "print_s" "(" STRING_LITERAL ")" ";" | "print_c" "("
		 * lexp ")" ";" | "print_i" "(" lexp ")" ";" | funcall ";" | "read_c"
		 * "(" ")" ";" | "read_i" "(" ")" ";"
		 */
		// System.out.printf("parsing STATEMENTS parsing ON : %s \n",
		// token.tokenClass.toString());
		if (accept(TokenClass.LBRA)) {
			nextToken();
			List<VarDecl> variables = parseVarDecls();
			List<Stmt> statements = parseStatementRep();
			expect(TokenClass.RBRA);
			return new Block(variables, statements);
		} else if (accept(TokenClass.WHILE)) {
			nextToken();
			expect(TokenClass.LPAR);
			Expr condition = parseExp();
			expect(TokenClass.RPAR);
			Stmt actions = parseStatement();
			return new While(condition, actions);
		} else if (accept(TokenClass.IF)) {
			nextToken();
			expect(TokenClass.LPAR);
			Expr condition = parseExp();
			expect(TokenClass.RPAR);
			Stmt actions = parseStatement();
			Stmt actionsOpt = parseElseOpt();
			return new If(condition, actionsOpt, actions);
		} else if (accept(TokenClass.IDENTIFIER) && !isFuncall()) {
			Var var = new Var(token.data);
			nextToken();
			expect(TokenClass.ASSIGN);
			Expr expression = parseLexp();
			expect(TokenClass.SEMICOLON);
			return new Assign(expression, var);
		} else if (accept(TokenClass.RETURN)) {
			nextToken();
			Expr expression = parseLexpOpt();
			expect(TokenClass.SEMICOLON);
			return new Return(expression);
		} else if (accept(TokenClass.PRINT)) {
			String data = token.data;
			nextToken();
			List<Expr> arguments = new ArrayList<>();
			switch (data) {
			case "print_s": {
				expect(TokenClass.LPAR);
				Token parameter = token;
				expect(TokenClass.STRING_LITERAL);
				StrLiteral argument = new StrLiteral(parameter.data);
				arguments.add(argument);
				expect(TokenClass.RPAR);
				expect(TokenClass.SEMICOLON);
				return new FunCallStmt(data, arguments);
			}
			case "print_c": {
				expect(TokenClass.LPAR);

				Expr argument = parseLexp();
				arguments.add(argument);
				expect(TokenClass.RPAR);
				expect(TokenClass.SEMICOLON);
				return new FunCallStmt(data, arguments);
			}
			case "print_i": {
				expect(TokenClass.LPAR);
				Expr argument = parseLexp();
				arguments.add(argument);
				expect(TokenClass.RPAR);
				expect(TokenClass.SEMICOLON);
				return new FunCallStmt(data, arguments);
			}

			}
		} else if (accept(TokenClass.READ)) {
			String data = token.data;
			List<Expr> arguments = new ArrayList<>();
			nextToken();
			expect(TokenClass.LPAR);
			expect(TokenClass.RPAR);
			expect(TokenClass.SEMICOLON);
			return new FunCallStmt(data, arguments);
		} else {
			// funcall ::= IDENT "(" ident_opt ")"
			Stmt statement = parseFuncallStmt();
			expect(TokenClass.SEMICOLON);
			return statement;
		}
		// should never be reached
		return null;
	}

	private Stmt parseFuncallStmt() {

		Token identifier = token;
		expect(TokenClass.IDENTIFIER);
		List<Expr> arguments = new ArrayList<>();
		expect(TokenClass.LPAR);
		if (accept(TokenClass.IDENTIFIER)) {
			arguments.addAll(parseArguments());
		}
		expect(TokenClass.RPAR);

		return new FunCallStmt(identifier.data, arguments);
	}

	private Expr parseFuncallExpr() {

		Token identifier = token;
		expect(TokenClass.IDENTIFIER);
		List<Expr> arguments = new ArrayList<>();
		expect(TokenClass.LPAR);
		if (accept(TokenClass.IDENTIFIER)) {
			arguments.addAll(parseArguments());
		}
		expect(TokenClass.RPAR);

		return new FunCallExpr(identifier.data, arguments);
	}

	private boolean isFuncall() {
		boolean result = accept(TokenClass.IDENTIFIER);
		result = result && lookAhead(1).tokenClass == TokenClass.LPAR;
		return result;
	}

	private Expr parseExp() {
		/*
		 * exp ::= lexp ">" lexp | lexp "<" lexp | lexp ">=" lexp | lexp "<="
		 * lexp | lexp "!=" lexp | lexp "==" lexp | lexp
		 */
		Expr expr = parseLexp();
		if (accept(TokenClass.LE, TokenClass.GE, TokenClass.LT, TokenClass.GT, TokenClass.NE, TokenClass.EQ)) {
			Op op = null;
			if (TokenClass.LE == token.tokenClass) {
				op = Op.LE;
			}
			if (TokenClass.GE == token.tokenClass) {
				op = Op.GE;
			}
			if (TokenClass.LT == token.tokenClass) {
				op = Op.LT;
			}
			if (TokenClass.GT == token.tokenClass) {
				op = Op.GT;
			}
			if (TokenClass.NE == token.tokenClass) {
				op = Op.NE;
			}
			if (TokenClass.EQ == token.tokenClass) {
				op = Op.EQ;
			}
			nextToken();
			return new BinOp(expr, op, parseLexp());
		}
		return expr;
	}

	private Expr parseLexp() {
		Expr expr = parseTerm();
		Op op = null;
		if (accept(TokenClass.PLUS, TokenClass.MINUS)) {
			op = (token.tokenClass == TokenClass.PLUS) ? Op.ADD : Op.SUB;
			return new BinOp(expr, op, parseTermRep());
		}
		return expr;

	}

	private Expr parseTerm() {

		Expr expr = parseFactor();
		Op op = null;
		if (accept(TokenClass.MOD, TokenClass.DIV, TokenClass.TIMES)) {
			if (token.tokenClass == TokenClass.MOD) {
				op = Op.MOD;
			}
			if (token.tokenClass == TokenClass.DIV) {
				op = Op.DIV;
			}
			if (token.tokenClass == TokenClass.TIMES) {
				op = Op.MUL;
			}
			return new BinOp(expr, op, parseFactorRep());
		}
		return expr;

	}

	private Expr parseTermRep() {
		if (accept(TokenClass.PLUS, TokenClass.MINUS)) {
			nextToken();
			return parseLexp();
		}
		return null;
		// term_rep :: = term_rep (("+"|"-") term) | ϵ
	}

	private Expr parseLexpOpt() {
		// System.out.printf("parsing LEXP_OPT parsing ON : %s \n",
		// token.tokenClass.toString());
		// checking as normal factor
		if (accept(TokenClass.LPAR, TokenClass.CHARACTER, TokenClass.MINUS, TokenClass.IDENTIFIER, TokenClass.NUMBER, TokenClass.READ)
				|| isFuncall()) {
			return parseLexp();
		}
		return null;
		// System.out.printf("parsing LEXP_OPT finished ON : %s \n",
		// token.tokenClass.toString());
	}

	private Expr parseFactor() {
		/*
		 * factor ::= "(" lexp ")" | minus_opt (IDENT|NUMBER) | CHARACTER |
		 * funcall | "read_c" "(" ")" | "read_i" "(" ")"
		 */
		// System.out.printf("parsing FACTOR parsing ON : %s \n",
		// token.tokenClass.toString());
		if (accept(TokenClass.LPAR)) {
			nextToken();
			Expr expr = parseLexp();
			expect(TokenClass.RPAR);
			return expr;
		} else if (accept(TokenClass.MINUS)) {
			nextToken();
			Token typeId = token;
			expect(TokenClass.IDENTIFIER, TokenClass.NUMBER);
			if (typeId.tokenClass == TokenClass.NUMBER) {
				return new BinOp(new IntLiteral(0), Op.SUB, new IntLiteral(Integer.parseInt(typeId.data)));
			} else {
				return new BinOp(new IntLiteral(0), Op.SUB, new Var(typeId.data));
			}
		} else if (accept(TokenClass.IDENTIFIER, TokenClass.NUMBER) && !isFuncall()) {
			Token typeId = token;
			expect(TokenClass.IDENTIFIER, TokenClass.NUMBER);
			if (typeId.tokenClass == TokenClass.NUMBER) {
				return new IntLiteral(Integer.parseInt(typeId.data));
			} else {
				return new Var(typeId.data);
			}
		} else if (accept(TokenClass.CHARACTER)) {
			Token typeId = token;
			expect(TokenClass.CHARACTER);
			return new ChrLiteral(typeId.data.charAt(0));
		} else if (accept(TokenClass.READ)) {
			Token typeId = token;
			expect(TokenClass.READ);
			expect(TokenClass.LPAR);
			expect(TokenClass.RPAR);
			return new FunCallExpr(typeId.data, new ArrayList<Expr>());
		} else {
			return parseFuncallExpr();
		}

	}

	private Expr parseFactorRep() {
		if (accept(TokenClass.MOD, TokenClass.DIV, TokenClass.TIMES)) {
			nextToken();
			return parseTerm();
		}
		return null;
		// factor_rep :: factor_rep (("/"|"*"|"%") factor) | ϵ
	}

	private List<Expr> parseArguments() {
		List<Expr> result = new ArrayList<>();
		Token argument = token;
		expect(TokenClass.IDENTIFIER);
		result.add(new Var(argument.data));
		if (accept(TokenClass.COMMA)) {
			nextToken();
			result.addAll(parseArguments());
		}
		return result;
	}

	private Stmt parseElseOpt() {
		if (accept(TokenClass.ELSE)) {
			nextToken();
			return parseStatement();
		}
		return null;
	}

	private Procedure parseMain() {
		expect(TokenClass.VOID);
		expect(TokenClass.MAIN);
		expect(TokenClass.LPAR);
		expect(TokenClass.RPAR);
		Block block = parseBody();
		return new Procedure(Type.VOID, "main", new ArrayList<VarDecl>(), block);
		// "void" "main" "(" ")" body
	}
}