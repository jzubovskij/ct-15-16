package ast;

public class Var extends Expr {
    public final String name;
    VarDecl vd;
    public VarDecl getVd() {
		return vd;
	}

	public void setVd(VarDecl vd) {
		this.vd = vd;
	}

	public Var(String name){
	this.name = name;
    }

    public <T> T accept(ASTVisitor<T> v) {
    	
	    return v.visitVar(this);
    }
}
