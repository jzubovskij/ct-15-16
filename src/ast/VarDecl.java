package ast;

public class VarDecl implements Tree {
    public final Type type;
    public final Var var;
    public boolean global = false;
    public int index;

    public VarDecl(Type type, Var var) {
	this.type = type;
	this.var = var;
    }

     public <T> T accept(ASTVisitor<T> v) {
	return v.visitVarDecl(this);
    }
     
     public String getType()
     {
    	 if(type == Type.CHAR)
    	 {
    		 return "C";
    	 }
    	 if(type == Type.INT)
    	 {
    		 return "I";
    	 }
    	 if(type == Type.STRING)
    	 {
    		 return  "Ljava/lang/String;";
    	 }
    	 if(type == Type.VOID)
    	 {
    		 return  "V";
    	 }
    	 return "stuff";
     }
}
