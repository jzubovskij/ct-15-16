package ast;

public class BinOp  extends Expr{
	public final Expr operandOne;
	public final Op operator;
	public final Expr operandTwo;
	public BinOp(Expr operandOne, Op operator, Expr operandTwo)
	{
		this.operator = operator;
		this.operandOne = operandOne;
		this.operandTwo = operandTwo;
	}
	public <T> T accept(ASTVisitor<T> v) {
	    return v.visitBinOp(this);
    }
}
