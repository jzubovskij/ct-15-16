package ast;

public class Assign extends Stmt{
	public final Var var;
	public final Expr expression;
	public Assign(Expr expression, Var var)
	{
		this.var = var;
		this.expression = expression;
	}
	public <T> T accept(ASTVisitor<T> v) {
	    return v.visitAssign(this);
    }
}
