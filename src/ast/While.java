package ast;

public class While extends Stmt{
	public final Expr condition;
	public final Stmt actions;
	public While(Expr condition, Stmt actions)
	{
		this.condition = condition;
		this.actions = actions;
	}
	public <T> T accept(ASTVisitor<T> v) {
	    return v.visitWhile(this);
    }
}
