package ast;

import java.util.List;

public class FunCallExpr extends Expr {
	public final String name;
	public final List<Expr> arguments;

	Procedure p;

	public Procedure getP() {
		return p;
	}

	public void setP(Procedure p) {
		this.p = p;
	}

	public FunCallExpr(String name, List<Expr> arguments) {
		this.name = name;
		this.arguments = arguments;
	}

	public <T> T accept(ASTVisitor<T> v) {
		return v.visitFunCallExpr(this);
	}
}
