package ast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ASTPrinter implements ASTVisitor<Void> {

	private PrintWriter writer;

	public ASTPrinter(PrintWriter writer) {
		this.writer = writer;
	}

	public Void visitProcedure(Procedure p) {
		writer.print("Procedure(");
		writer.print(p.type);
		writer.print("," + p.name + ",");
		for (VarDecl vd : p.params) {

			vd.accept(this);
			writer.print(",");
		}
		p.block.accept(this);
		writer.print(")");
		return null;
	}

	public Void visitProgram(Program p) {
		writer.print("Program(");
		int counter = 0;
		for (VarDecl vd : p.varDecls) {
			if (counter != 0)
				writer.print(",");
			counter++;
			vd.accept(this);

		}

		if (counter != 0 && !p.procs.isEmpty())
		{
			writer.print(",");
			counter = 0;
		}
		for (Procedure proc : p.procs) {
			if (counter != 0)
				writer.print(",");
			counter++;
			proc.accept(this);

		}
		if (counter != 0)
			writer.print(",");
		p.main.accept(this);
		writer.print(")");
		writer.flush();
		return null;
	}

	public Void visitVarDecl(VarDecl vd) {
		writer.print("VarDecl(");
		writer.print(vd.type + ",");
		vd.var.accept(this);
		writer.print(")");
		return null;
	}

	public Void visitVar(Var v) {
		writer.print("Var(");
		writer.print(v.name);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitChrLiteral(ChrLiteral chrLiteral) {
		writer.print("ChrLiteral(");
		writer.print(chrLiteral.character);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral strLiteral) {
		writer.print("StrLiteral(");
		writer.print(strLiteral.string);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral intLiteral) {
		writer.print("IntLiteral(");
		writer.print(intLiteral.integer);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitBinOp(BinOp binOp) {
		writer.print("BinOp(");
		binOp.operandOne.accept(this);
		writer.print(",");
		writer.print(binOp.operator);
		writer.print(",");
		binOp.operandTwo.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr funCallExpr) {
		writer.print("FunCallExpr(");
		writer.print(funCallExpr.name);
		for (Expr expr : funCallExpr.arguments) {
			writer.print(",");
			expr.accept(this);
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitFunCallStmt(FunCallStmt funCallStmt) {
		writer.print("FunCallStmt(");
		writer.print(funCallStmt.name);
		for (Expr expr : funCallStmt.arguments) {
			writer.print(",");
			expr.accept(this);
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitWhile(While while1) {
		writer.print("While(");
		while1.condition.accept(this);
		writer.print(",");
		while1.actions.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitIf(If if1) {
		writer.print("If(");
		if1.condition.accept(this);
		if (if1.actions != null) {
			writer.print(",");
			if1.actions.accept(this);
		}
		if (if1.elseCondition != null) {
			writer.print(",");
			if1.elseCondition.accept(this);
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitAssign(Assign assign) {
		writer.print("Assign(");
		assign.var.accept(this);
		writer.print(",");
		assign.expression.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitReturn(Return return1) {
		writer.print("Return(");
		if (return1.expr != null) {
			return1.expr.accept(this);
		}
		writer.print(")");
		return null;
	}

	public Void visitBlock(Block b) {
		writer.print("Block(");
		int counter = 0;
		for (VarDecl vd : b.variables) {
			if (counter != 0)
				writer.print(",");
			vd.accept(this);
			counter++;
		}
		if (counter != 0 && !b.statements.isEmpty())
			writer.print(",");
		counter = 0;

		counter = 0;

		for (Stmt stmt : b.statements) {
			if (counter != 0)
				writer.print(",");
			stmt.accept(this);
			counter++;
		}
		writer.print(")");
		return null;
	}
}
