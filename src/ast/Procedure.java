package ast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Procedure implements Tree {

	public final Type type;
	public final String name;
	public final List<VarDecl> params;
	public final Block block;

	public int index;
	public final Map<Integer, VarDecl> variables;

	public Procedure(Type type, String name, List<VarDecl> params, Block block) {
		this.type = type;
		this.name = name;
		this.params = params;
		this.block = block;
		if (name.equals("main")) {
			this.index = 1;
		} else
			this.index = 0;
		this.variables = new HashMap<Integer, VarDecl>();
	}

	public <T> T accept(ASTVisitor<T> v) {
		return v.visitProcedure(this);
	}

	public void putVardiableDeclaration(VarDecl vd) {
		variables.put(index, vd);
		index++;

	}

	public VarDecl getVariableIndex(int location) {
		return variables.get(location);
	}

	public String getType() {
		if (type == Type.CHAR) {
			return "C";
		}
		if (type == Type.INT) {
			return "I";
		}
		if (type == Type.STRING) {
			return "Ljava/lang/String;";
		}
		if (type == Type.VOID) {
			return "V";
		}
		return "stuff";
	}

	public int getParameterCount() {
		return params.size();
	}

	public void printVariables() {
		Map<Integer, VarDecl> map = variables;

		for (int key : map.keySet()) {
			System.out.println("key : " + key + " value : " + map.get(key).var.name);
		}
	}

}
