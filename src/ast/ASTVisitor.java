package ast;

public interface ASTVisitor<T> {
    public T visitBlock(Block b);
    public T visitProcedure(Procedure p);
    public T visitProgram(Program p);
    public T visitVarDecl(VarDecl vd);
    public T visitVar(Var v);
	public T visitChrLiteral(ChrLiteral chrLiteral);
	public T visitStrLiteral(StrLiteral strLiteral);
	public T visitIntLiteral(IntLiteral intLiteral);
	public T visitBinOp(BinOp binOp);
	public T visitFunCallExpr(FunCallExpr funCallExpr);
	public T visitWhile(While while1);
	public T visitIf(If if1);
	public T visitAssign(Assign assign);
	public T visitReturn(Return return1);
	public T visitFunCallStmt(FunCallStmt funCallStmt);

    // to complete ... (should have one visit method for each concrete AST node class)
}
