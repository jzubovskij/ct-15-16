package ast;

public class If extends Stmt{
	public final Expr condition;
	public final Stmt actions;
	//this one can be null
	public final Stmt elseCondition;
	public If(Expr condition, Stmt elseCondition, Stmt actions)
	{
		this.condition = condition;
		this.actions = actions;
		this.elseCondition = elseCondition;
	}
	public <T> T accept(ASTVisitor<T> v) {
	    return v.visitIf(this);
    }
}
