package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;

public class Tokeniser {
	private Scanner scanner;
	int line = 0, column = 0;// current character

	private int error = 0;
	char c;

	public int getErrorCount() {
		return this.error;
	}

	public Tokeniser(Scanner scanner) {
		this.scanner = scanner;
	}

	private void error(char c, int line, int col) {
		System.out.println("Lexing error: unrecognised character (" + c + ") at " + line + ":" + col);
		error++;
	}

	public Token nextToken() {
		Token result;
		try {
			result = next();
		} catch (EOFException eof) {
			// end of file, nothing to worry about, just return EOF token
			return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
		} catch (IOException ioe) {
			ioe.printStackTrace();
			// something went horribly wrong, abort
			System.exit(-1);
			return null;
		}
		return result;
	}

	private Token next() throws IOException {
		line = scanner.getLine();
		column = scanner.getColumn();

		c = scanner.next();

		// skip white spaces
		if (Character.isWhitespace(c)) {
			// go to next token
			return next();
		}

		else if (c == '/') {
			// skip single line comment
			if (scanner.peek() == '/') {
				return singleLineComment();
				// slip multiple line comment
			} else if (scanner.peek() == '*') {
				return multiLineComment();
			}
			// otherwise just division
			else {
				return new Token(TokenClass.DIV, line, column);
			}
		}

		// recognize includes
		else if (c == '#') {
			return include();
		}

		// recognizes string literal
		else if (c == '"') {
			return stringLiteral();
		}

		// recognizes number
		else if (Character.isDigit(c)) {
			return number();
		}
		// recognizes character literal
		else if (c == '\'') {
			return characterLiteratl();
		}

		// recognizes identifiers and other keywords
		else if (Character.isLetter(c) || c == '_') {
			return identifierOrKeyword();
		}

		// recognizes comparisons and assign
		else if (c == '=') {
			c = scanner.peek();
			if (c == '=') {
				c = scanner.next();
				return new Token(TokenClass.EQ, line, column);
			} else
				// recognizes assignments
				return new Token(TokenClass.ASSIGN, line, column);
		} else if (c == '!') {
			c = scanner.peek();
			if (c == '=') {
				c = scanner.next();
				return new Token(TokenClass.NE, line, column);
			}
		} else if (c == '<') {
			c = scanner.peek();
			if (c == '=') {
				c = scanner.next();
				return new Token(TokenClass.LE, line, column);
			} else {
				return new Token(TokenClass.LT, line, column);
			}
		} else if (c == '>') {
			c = scanner.peek();
			if (c == '=') {
				c = scanner.next();
				return new Token(TokenClass.GE, line, column);
			} else {
				return new Token(TokenClass.GT, line, column);
			}
		}

		// recognizes arithmetic operators
		else if (c == '+') {
			return new Token(TokenClass.PLUS, line, column);
		} else if (c == '-') {
			return new Token(TokenClass.MINUS, line, column);
		} else if (c == '*') {
			return new Token(TokenClass.TIMES, line, column);
		} else if (c == '/') {
			return new Token(TokenClass.DIV, line, column);
		} else if (c == '%') {
			return new Token(TokenClass.MOD, line, column);
		}

		// recognizes delimiters
		else if (c == '{') {
			return new Token(TokenClass.LBRA, line, column);
		} else if (c == '}') {
			return new Token(TokenClass.RBRA, line, column);
		} else if (c == '(') {
			return new Token(TokenClass.LPAR, line, column);
		} else if (c == ')') {
			return new Token(TokenClass.RPAR, line, column);
		} else if (c == ';') {
			return new Token(TokenClass.SEMICOLON, line, column);
		} else if (c == ',') {
			return new Token(TokenClass.COMMA, line, column);
		}

		// if we reach this point, it means we did not recognize a valid token
		return invalid();
	}

	private Token identifierOrKeyword() throws IOException {
		String data = "";
		data += c;
		c = scanner.peek();
		while (Character.isLetter(c) || Character.isDigit(c) || c == '_') {
			c = scanner.next();
			data += c;
			c = scanner.peek();
		}

		// check if it is a keyword
		if (data.equals("int"))
			return new Token(TokenClass.INT, line, column);
		else if (data.equals("char"))
			return new Token(TokenClass.CHAR, line, column);
		else if (data.equals("void"))
			return new Token(TokenClass.VOID, line, column);
		else if (data.equals("if"))
			return new Token(TokenClass.IF, line, column);
		else if (data.equals("else"))
			return new Token(TokenClass.ELSE, line, column);
		else if (data.equals("while"))
			return new Token(TokenClass.WHILE, line, column);
		else if (data.equals("return"))
			return new Token(TokenClass.RETURN, line, column);
		else if (data.equals("main"))
			return new Token(TokenClass.MAIN, line, column);
		else if (data.equals("read_i") || data.equals("read_c")) {
			return new Token(TokenClass.READ, data, line, column);
		} else if (data.equals("print_i") || data.equals("print_c") || data.equals("print_s")) {
			return new Token(TokenClass.PRINT, data, line, column);
		} else
			return new Token(TokenClass.IDENTIFIER, data, line, column);

	}

	private Token include() throws IOException {
		String data = "";
		try {

			for (int i = 0; i < 7; i++) {
				c = scanner.next();
				data += c;
				c = scanner.peek();
			}
			if (data.equals("include")) {
				return new Token(TokenClass.INCLUDE, line, column);
			} else {
				return invalid();
			}
		} catch (EOFException eof) {
			// malformed include token, return invalid token
			return invalid();
		}

	}

	private Token multiLineComment() throws IOException {
		// skip until comment ends
		try {
			c = scanner.next();
			c = scanner.next();
		} catch (EOFException eof) {
			// unclosed comment, return invalid token
			return invalid();
		}
		// looking at potentially /**/ as an empty comment
		while (!(c == '*' && (scanner.peek() == '/'))) {
			try {
				c = scanner.next();
			} catch (EOFException eof) {
				// unclosed comment, return invalid token
				return invalid();
			}
		}
		// point at next character (of the next token)
		try {
			c = scanner.next();
		} catch (EOFException eof) {
			// unclosed comment, return invalid token
			return invalid();
		}
		return next();
	}

	private Token singleLineComment() throws IOException {
		while (!(c == '\n' || c == '\r')) {
			c = scanner.next();
		}
		return next();
	}

	private Token number() throws IOException {
		String data = "";
		data += c;
		c = scanner.peek();
		while (Character.isDigit(c)) {
			c = scanner.next();
			data += c;
			c = scanner.peek();
		}
		return new Token(TokenClass.NUMBER, data, line, column);
	}

	private Token stringLiteral() throws IOException {
		boolean invalid = false;
		String data = "";
		// read first character in the string
		c = scanner.peek();
		// reading until next line
		while (c != '"') {
			if (c == '\n' || c == '\r') {
				// Sting was not closed and we only support 1 line String
				return invalid();
			}
			// c is now the next (or first) string character)
			try {
				c = scanner.next();
			} catch (EOFException eof) {
				// unclosed String, return invalid token
				return invalid();
			}
			if (c == '\\') {
				try {
					c = scanner.next();
				} catch (EOFException eof) {
					// unclosed String, return invalid token
					return invalid();
				}
				// is is now the escape character
				String escapeChar = escapeCharacter("\\" + c);
				// c is now the next character after the escape
				if (!escapeChar.equals("invalid")) {
					data += escapeChar;
				} else {
					// not a supported escape character
					invalid = true;
					// not returning INVALID yet to process full string
				}
			}

			else // not an escape character
			{
				data += c;
			}
			c = scanner.peek();
		}
		// point to the string delimiter
		try {
			c = scanner.next();
		} catch (EOFException eof) {
			// unclosed String, return invalid token
			return invalid();
		}
		if (invalid) {
			return invalid();
		}
		return new Token(TokenClass.STRING_LITERAL, data, line, column);
	}

	private Token characterLiteratl() throws IOException {
		String data = "";
		c = scanner.peek();
		boolean escape = false;
		while (c != '\'' || escape) {
			if (c == '\n' || c == '\r') {
				// character was not closed and we only support 1 line String
				return invalid();
			}
			try {
				c = scanner.next();
				// if it is an escape sequence
				if (c == '\\') {
					// so a double // does not cause a bug
					escape = !escape;
				} else {
					// otherwise we read the escape character
					escape = false;
				}
				data += c;
				c = scanner.peek();
			} catch (EOFException eof) {
				// unclosed character, return invalid token
				return invalid();
			}
		}
		try {
			// point to character delimiter
			c = scanner.next();
		} catch (EOFException eof) {
			// unclosed String, return invalid token
			return invalid();
		}
		if (data.length() != 1) {
			String escapeChar = escapeCharacter(data);
			if (!escapeChar.equals("invalid")) {
				return new Token(TokenClass.CHARACTER, escapeChar, line, column);
			} else {
				// not a supported escape character
				return invalid();
			}
		} else
			return new Token(TokenClass.CHARACTER, data, line, column);

	}

	private String escapeCharacter(String escapeChar) { // "\\n" or "\\'" etc...
		// if not a supported escape character
		String data = "invalid";
		if (escapeChar.equals("\\t")) {
			data = "\t";
		} else if (escapeChar.equals("\\\\")) {
			data = "\\";
		} else if (escapeChar.equals("\\'")) {
			data = "\'";
		} else if (escapeChar.equals("\\\"")) {
			data = "\"";
		} else if (escapeChar.equals("\\n")) {
			data = "\n";
		} else if (escapeChar.equals("\\r")) {
			data = "\r";
		} else if (escapeChar.equals("\\b")) {
			data = "\b";
		} else if (escapeChar.equals("\\f")) {
			data = "\f";
		}
		// return invalid if NOT one of the supported chars
		return data;
	}

	private Token invalid() {
		error(c, line, column);
		return new Token(TokenClass.INVALID, line, column);
	}
}
